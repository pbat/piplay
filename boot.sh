#! /bin/bash

/usr/sbin/rmmod snd_bcm2835 >> /home/pietro/logs/piplay_boot.log 2>&1
/usr/sbin/modprobe snd_bcm2835 enable_headphones  >> /home/pietro/logs/piplay_boot.log 2>&1

/home/pietro/piplay/initialize.py  >> /home/pietro/logs/piplay_boot.log 2>&1

#su -u pietro ?

source /home/pietro/piplay_env/bin/activate >> /home/pietro/logs/piplay_boot.log 2>&1
cd /home/pietro/piplay
nice -n -5 ./piplay.py --boot >> ../logs/piplay.log  2>&1 &
nice -n 5 python3 config_server.py >> ../logs/config_server.log 2>&1
