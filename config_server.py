from http.server import BaseHTTPRequestHandler, HTTPServer
import subprocess
import urllib.parse

class VolumeHandler(BaseHTTPRequestHandler):
    def get_current_volume(self):
        cur = (subprocess.check_output(['amixer', 'get', 'PCM'])
                         .decode('utf-8')
                         .splitlines()
                         [-1]
                         .split('[')
                         [1]
                         [:-3])
        return cur
        

    def do_GET(self):
        if self.path == '/':
            cur = self.get_current_volume()
            self.send_response(200)
            self.send_header('Content-type', 'text/html')
            self.end_headers()
            self.wfile.write(f'''
                <html>
                <head><title>Volume Control</title></head>
                <body>
                <h1 style="width : 100%">Volume Control</h1>
                <form action="/set_volume" method="post"  style="width : 400px; text-align: center">
                    <input type="range" name="volume" min="40" max="100" value={cur} style="width : 400px">
                    <input type="submit" value="Set Volume" style="width : 200 px; text-align: center">
                </form>
                </body>
                </html>
            '''.encode('utf-8'))
        else:
            self.send_response(404)
            self.end_headers()
            self.wfile.write(b'Not Found')

    def do_POST(self):
        if self.path == '/set_volume':
            content_length = int(self.headers['Content-Length'])
            post_data = self.rfile.read(content_length)
            post_data = urllib.parse.parse_qs(post_data.decode('utf-8'))
            volume = int(post_data['volume'][0])
            subprocess.run(['amixer', 'set', 'PCM', f'{volume}%'])
            self.send_response(301)
            new_path = '/'
            self.send_header('Location', new_path)
            self.end_headers()
            self.wfile.write(b'Volume set successfully')

if __name__ == '__main__':
    server_address = ('0.0.0.0', 8000)  # Listen on all network interfaces
    httpd = HTTPServer(server_address, VolumeHandler)
    print('Volume control server running...')
    httpd.serve_forever()
