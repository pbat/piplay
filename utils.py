def hex_to_tuple(code, append_checksum=False):
    values = [int(code[i*2:i*2+2], 16) for i in range(int(len(code)/2))]
    if append_checksum:
        values.append(compute_checksum(code))
    return tuple(values)

def compute_checksum(uid):
    if isinstance(uid, str):
        uid = hex_to_tuple(uid)

    return uid[0]^uid[1]^uid[2]^uid[3]
    
def check_checksum(uid):
    if isinstance(uid, str):
        uid = hex_to_tuple(uid)

    checksum = compute_checksum(uid[:4])
    assert(checksum == uid[4])


if __name__ == '__main__':
    with open('../piplay_catalog.py') as fin:
        d = eval(fin.read())
        for k in d:
            check_checksum(k)
        print(f"Checked {len(d)} checksums")
