#! /usr/bin/env python

from pirc522 import RFID
import time
import sys
import subprocess
from collections import defaultdict

from configurer import Configurer

USE_VLC = True
if USE_VLC:
    import vlc
else:
    import gi
    gi.require_version('Gst', '1.0')
    from gi.repository import Gst, GObject

class Player():
    def __init__(self):
        if USE_VLC:
            self._instance = vlc.Instance()
            self._player = self._instance.media_player_new()
        else:
            Gst.init([])
            self._player = Gst.ElementFactory.make("playbin", "player")
            sink = Gst.ElementFactory.make("alsasink", "alsasink")
            self._player.set_property("audio-sink", sink)

        # FIXME: redundant, just easier to access than the player's state:
        self.state = 'stop'
        # TODO: capture when song ends and put state back to 'stop'

    def play(self, file_path=None):
        if file_path is not None:
            if USE_VLC:
                media = self._instance.media_new(file_path)
                self._player.set_media(media)
            else:
                self._player.set_state(Gst.State.NULL)
                self._player.set_property("uri", "file://" + file_path)

        if USE_VLC:
            self._player.play()
        else:
            self._player.set_state(Gst.State.PLAYING)

        self.state = 'play'


    def pause(self):
        if USE_VLC:
            self._player.pause()
        else:
            self._player.set_state(Gst.State.PAUSED)
        self.state = 'pause'


# FIXME: find user name
CATALOG_PATH = '/home/pietro/piplay_catalog.py'
SONGS_PATH = '/home/pietro/songs'

class PiPlayer:
    def __init__(self):
        self.rdr = RFID()

        self._player = Player()

        self.last_uid = None
        self.last_time = 0
        self.last_command = None

        self.load_catalog()

    def load_catalog(self):
        try:
            with open(CATALOG_PATH) as fin:
                self.catalog = eval(fin.read())
        except Exception as exc:
            print("Error loading catalog", exc)
            # Emergency! Everything is a config key.
            self.catalog = defaultdict(lambda : 'CONFIG_KEY')

    def wait_for_tag(self):
        self.play_path('assets/jingle.ogg')
        # Just the duration of the jingle:
        time.sleep(2)

        while True:
            now = time.time()
            self.rdr.wait_for_tag(.2)

            (error, tag_type) = self.rdr.request()
            if error:
                self._process_no_uid()
                continue

            (error, uid) = self.rdr.anticoll()
            if error:
                print("Error in anticoll", error)
                continue
            
            self._process_uid(tuple(uid))
            time.sleep(.2)

    def _process_uid(self, uid):
        if self.last_uid == uid:
            if self._player.state == 'pause':
                self._player.play()
            return

        song = self.catalog.get(uid, None)
        if song is None:
            print("Unknown tag!", uid)
            return

        print(song)

        if song == 'CONFIG_KEY':
            self.beep()
            Configurer(self).run()
            return

        file_path = f'{SONGS_PATH}/{song}'
        self.play_path(file_path)

        self.last_uid = uid

    def play_path(self, file_path):
        self._player.play(file_path)
        
    def _process_no_uid(self):
        if self._player.state == 'play':
            print("Pause")
            self._player.pause()

    def beep(self):
        self.play_path('assets/singhiozzo.ogg')


if __name__ == '__main__':
    if '--boot' in sys.argv:
        print(sys.argv)
        time.sleep(5)

    player = PiPlayer()
    while True:
        try:
            player.wait_for_tag()
        except KeyboardInterrupt:
            # Calls GPIO cleanup
            player.rdr.cleanup()
            break
        except Exception as exc:
            # Uncaught exception... just try again:
            print("Exception", exc)
            print("Waiting for 10 seconds")
            time.sleep(10)

