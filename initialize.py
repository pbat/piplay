#! /usr/bin/env python

import os

def initialize():
    # These will only be needed the first time this program is ran... let's
    # avoid loading them all other times:
    from subprocess import call

    def run(cmd):
        call(cmd.split())

#    call('sudo apt install virtualenv vlc')
    run('sudo apt-get install -y virtualenv python3-gi gir1.2-gstreamer-1.0 '
        'gstreamer1.0-alsa vlc')

    run('sudo ln -s /home/pietro/piplay/systemd/piplay.service '
        '/etc/systemd/system/piplay.service')
    run('sudo systemctl enable piplay')

    os.chdir('/home/pietro')

    run('virtualenv piplay_env --system-site-packages')

    run('source piplay_env/bin/activate')
    run('pip install pi-rc522')
#    run('pip install python-vlc')

    with open('/home/pietro/piplay_catalog.py', 'w') as fout:
        fout.write('{\n\n}')

if __name__ == '__main__':
    env_dir = '/home/pietro/piplay_env'
    if not os.path.exists(env_dir):
        print(f"{env_dir} not found, initializing")
        initialize()


