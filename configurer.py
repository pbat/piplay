"""
This will eventually allow configuring the device without ssh-ing into it.
"""

LOG_FILE = '/home/pietro/logs/config.log'

import time
import sys
from subprocess import call

def log(*args):
    s = ' '.join([str(s) for s in args])
    # FIXME: shouldn't have to print _and_ write to file, just redirect
    print(s)
    with open(LOG_FILE, 'w', buffering=1) as fout:
        fout.write(s + '\n')

class Configurer:
    def __init__(self, piplayer):
        self._piplayer = piplayer

    def run(self):
        """
        For now, just write to stdout the id of any card read, and the
        associated song if any, and allow associating a song.
        Exit when the config key is read.
        """

        # Just give time to remove the config key, without immediately leaving
        # config
        log("Entering config in 2 seconds")
        self._piplayer.beep()

        time.sleep(2)

        log("Entered config")

        # Refresh key mappings:
        self._piplayer.load_catalog()

        # Shutdown if the first "count_to_shutdown" reads after entering
        # config are CONFIG_KEY
        count_to_shutdown = 5

        while True:
            self._piplayer.rdr.wait_for_tag()

            (error, tag_type) = self._piplayer.rdr.request()

            if error:
                continue

            (error, uid) = self._piplayer.rdr.anticoll()
            if error:
                log("Error in anticoll", error)
                continue

            uid = tuple(uid)

            song = self._piplayer.catalog.get(uid, None)

            log("Read card", uid, song)

            if song == 'CONFIG_KEY':
                if count_to_shutdown > 0:
                    count_to_shutdown -= 1
                    self._piplayer.beep()
                    log(f"{count_to_shutdown} to shutdown")
                    if not count_to_shutdown:
                        log("Shutdown")
                        call(['sudo', '/usr/sbin/halt'])
                        sys.exit(0)
                else:
                    log("Leaving config in 2 seconds")
                    time.sleep(2)
                    self._piplayer.load_catalog()
                    log("Back to normal")
                    self._piplayer.beep()
                    break
            else:
                count_to_shutdown = -1

            time.sleep(1)

